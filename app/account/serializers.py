from rest_framework import serializers
from core.models import UserBankAccount, Transaction

class AccountSerializer(serializers.ModelSerializer):

    transactions = serializers.PrimaryKeyRelatedField(
        many = True,
        read_only=True
        #queryset = Transaction.objects.all()
    )

    def create(self,data):
        account = UserBankAccount.objects.create_account(user=self.context['request'].user)
        return account

    class Meta:
        model = UserBankAccount
        fields = ('id','account_no','balance','user','transactions')
        read_only_fields = ('account_no','balance','user','transactions')

class TransactionSerializer(serializers.ModelSerializer):

    def create(self,data):
        
        print('retreiving account')
        account = data['account']
     
        try:
            amount = int(data['amount'])
            transaction_type = int(data['transaction_type'])
        except:
            raise ValueError('Invalid number or incorrect amount')
        transaction =  Transaction.objects.create_transaction(account,amount,transaction_type)
        return transaction

    class Meta:
        model = Transaction
        fields = ('amount','transaction_type','timestamp','balance_after_transaction','account')
        read_only_fields = ('balance_after_transaction',)

class AccountDetailSerializer(AccountSerializer):
    transactions = TransactionSerializer(many=True, read_only=True,required=False)