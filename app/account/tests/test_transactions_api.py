from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse

from rest_framework import status
from rest_framework.test import APIClient

from core.models import UserBankAccount, Transaction

from account.serializers import TransactionSerializer


TRANSACTIONS_URL = reverse('account:transaction-list')


def get_sample_account(user):
    account = UserBankAccount.objects.create_account(
            user =  user
    )
    return account


class PublicTransactionsApiTests(TestCase):

    def setUp(self):
        self.client = APIClient()

    def test_auth_required(self):
        """Test that authentication is required"""
        res = self.client.get(TRANSACTIONS_URL)

        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)

class PrivateTransactionsApiTests(TestCase):

    def setUp(self):
        self.client = APIClient()
       
        
        self.account = UserBankAccount.objects.create_account(
            user=get_user_model().objects.create_user(
            'test@moneyapi.com',
            'testpass'
        ))
        self.account.save()
        
        self.client.force_authenticate(self.account.user)

    def test_retrieve_transaction_list(self):
        """Test retrieving a list of transactions"""
        Transaction.objects.create(account=self.account, amount=100,transaction_type=1)
        Transaction.objects.create(account=self.account, amount=50, transaction_type=2)

        res = self.client.get(TRANSACTIONS_URL)

        transactions = Transaction.objects.all().order_by('-timestamp')
        serializer = TransactionSerializer(transactions, many=True)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, serializer.data)


    def test_transactions_limited_to_user(self):
        user2 = get_user_model().objects.create_user(
            'other@moneyapi.com',
            'testpass'
        )
        account2 = UserBankAccount.objects.create_account(user=user2)
        Transaction.objects.create_transaction(account=account2, amount=100,transaction_type=1)
        transaction = Transaction.objects.create_transaction(account=self.account, amount=50,transaction_type=1)

        res = self.client.get(TRANSACTIONS_URL)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(len(res.data), 1)


    def test_create_transaction_successful(self):
        """Test create a new transaction"""
        payload = {'amount': '0','transaction_type':'1','account':self.account.id}
        response = self.client.post(TRANSACTIONS_URL, payload)

        exists = Transaction.objects.filter(
            account=self.account,
            amount=int(payload['amount']),
            transaction_type=int(payload['transaction_type'])
        ).exists()
        self.assertTrue(exists)



 
   
    