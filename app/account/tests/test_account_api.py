from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse

from rest_framework import status
from rest_framework.test import APIClient

from core.models import UserBankAccount, Transaction

from account.serializers import TransactionSerializer, AccountSerializer, AccountDetailSerializer

ACCOUNTS_URL = reverse('account:account-list')

def sample_account(user):
    """Create and return a sample account
    """
    account = UserBankAccount.objects.create_account(user=user)
    return account
    
def detail_url(account_id):
    """Return account detail URL"""
    return reverse('account:account-detail', args=[account_id])


def sample_transaction(account, amount=0, transaction_type=1):
    return Transaction.objects.create_transaction(account=account, amount=amount, transaction_type=transaction_type)

class PublicAccountApiTests(TestCase):

    def setUp(self):
        self.client = APIClient()

    def test_auth_required(self):
        """Test that authentication is required"""
        res = self.client.get(ACCOUNTS_URL)

        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)


class PrivateAccountApiTests(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.user = get_user_model().objects.create_user(
            'test@moneyapi.com',
            'testpass'
        )
        self.client.force_authenticate(self.user)

    def test_retrieve_accounts(self):
        """Test retrieving a list of accounts"""
        sample_account(user=self.user)
        sample_account(user=self.user)

        res = self.client.get(ACCOUNTS_URL)

        accounts = UserBankAccount.objects.all().order_by('-account_no')
        serializer = AccountSerializer(accounts, many=True)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        #self.assertEqual(res.data, serializer.data)

    def test_accounts_limited_to_user(self):
        """Test retrieving accounts for user"""
        user2 = get_user_model().objects.create_user(
            'other@moneyapi.com',
            'password123'
        )

        UserBankAccount.objects.create_account(user2)
        UserBankAccount.objects.create_account(self.user)
    
        res = self.client.get(ACCOUNTS_URL)

        accounts = UserBankAccount.objects.filter(user=self.user)
        serializer = AccountSerializer(accounts, many=True)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(len(res.data), 1)
        self.assertEqual(res.data, serializer.data)


    def test_view_account_detail(self):
        """Test viewing account detail"""
        account = sample_account(user=self.user)
        account.transactions.add(sample_transaction(account=account,amount=10,transaction_type=1))
        account.save()
        url = detail_url(account.id)
        res = self.client.get(url)

        serializer = AccountDetailSerializer(account)
        self.assertEqual(res.data, serializer.data)

