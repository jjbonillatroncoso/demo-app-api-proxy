from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets, mixins, status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
# Create your views here.

from core.models import Transaction, UserBankAccount

from account import serializers

class BaseAccountAttrViewSet(viewsets.GenericViewSet,
                            mixins.ListModelMixin,
                            mixins.CreateModelMixin):
    """Base viewset for user owned account attributes"""
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        """Return objects for the current authenticated user only"""
        assigned_only = bool(
            int(self.request.query_params.get('assigned_only', 0))
        )
        queryset = self.queryset
        if assigned_only:
            queryset = queryset.filter(account__isnull=False)

        return queryset.filter(
            account__user=self.request.user
        ).order_by('-timestamp').distinct()


class TransactionViewSet(BaseAccountAttrViewSet):
    """Manage transactions in the database"""
    queryset = Transaction.objects.all()
    serializer_class = serializers.TransactionSerializer

    def perform_create(self, serializer):
        """Create a new object"""
        print('save transaction')
        serializer.save()


class AccountViewSet(viewsets.ModelViewSet):
    
    serializer_class = serializers.AccountSerializer
    queryset = UserBankAccount.objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def _params_to_ints(self, qs):
        """Convert a list of string IDs to a list of integers"""
        return [int(str_id) for str_id in qs.split(',')]

    def get_queryset(self):
        """Retrieve the accounts for the authenticated user"""
        transactions = self.request.query_params.get('transactions')
        queryset = self.queryset
        if transactions:
            transactions_ids = self._params_to_ints(transactions)
            queryset = queryset.filter(transactions__id__in=transactions_ids)

        return queryset.filter(user=self.request.user)

    def get_serializer_class(self):
        """Return appropriate serializer class"""
        if self.action == 'retrieve':
            return serializers.AccountDetailSerializer

        return self.serializer_class

    def perform_create(self, serializer):
        """Create a new account"""
        serializer.save()