import uuid
import os
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, \
                                        PermissionsMixin
from django.core.validators import (
    MinValueValidator,
    MaxValueValidator,
)
from django.conf import settings

from decimal import Decimal
import uuid 

from account.constants import TRANSACTION_TYPE_CHOICES


def generate_account_id():
    return uuid.uuid4().hex[:6].upper()

class InvalidTransaction(Exception):
    pass


class UserManager(BaseUserManager):

    def create_user(self, email, password=None, **extra_fields):
        """Creates and saves a new user"""
        if not email:
            raise ValueError('Users must have an email address')
        user = self.model(email=self.normalize_email(email), **extra_fields)
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, password):
        """Creates and saves a new super user"""
        user = self.create_user(email, password)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)

        return user


class User(AbstractBaseUser, PermissionsMixin):
    """Custom user model that suppors using email instead of username"""
    email = models.EmailField(max_length=255, unique=True)
    name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'


class AccountManager(models.Manager):
    def create_account(self,user):
        if not user:
            raise ValueError('Accounts must have a user')
        account = self.model()
        account.balance = 0
        account.account_no = generate_account_id()
        account.user = user 
        account.save(using=self._db)
        return account


class UserBankAccount(models.Model):
    user = models.ForeignKey(
        User,
        on_delete='Cascade'
    )
  
    account_no = models.CharField(unique=True,max_length=255)
    balance = models.DecimalField(
        default=0,
        max_digits=12,
        decimal_places=2
    )

    objects = AccountManager()

    def __str__(self):
        return str(self.account_no)
        

    def process_transaction(self,transaction):
        if transaction.transaction_type == 2:
            if self.balance - transaction.amount < 0:
                raise InvalidTransaction('Insuficient funds')
            self.balance = self.balance - transaction.amount
        else:
            self.balance = self.balance + transaction.amount

        transaction.balance_after_transaction = self.balance  
        self.save()

        return transaction     


class TransactionManager(models.Manager):
    def create_transaction(self,account,amount,transaction_type):

        if amount < 0:
            raise ValueError('Invalid Amount')

        if not account:
            raise InvalidTransaction('Account required for user') 
      
        transaction = self.model(account=account,amount=amount,transaction_type=transaction_type)

        transaction = account.process_transaction(transaction)

        if not transaction:
            raise InvalidTransaction('Invalid Transaction')
        else:
            transaction.save(using=self._db)

        return transaction

class Transaction(models.Model):
    account = models.ForeignKey(
        UserBankAccount,
        related_name='transactions',
        on_delete=models.CASCADE,
    )
    amount = models.DecimalField(
        decimal_places=2,
        max_digits=12
    )
    balance_after_transaction = models.DecimalField(
        decimal_places=2,
        max_digits=12,
        null=True
    )
    transaction_type = models.PositiveSmallIntegerField(
        choices=TRANSACTION_TYPE_CHOICES
    )
    timestamp = models.DateTimeField(auto_now_add=True)

    objects = TransactionManager()    
    
    def __str__(self):
        return str(self.account.account_no)

    

    class Meta:
        ordering = ['timestamp']