# Generated by Django 2.1.15 on 2021-10-12 23:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_transaction'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userbankaccount',
            name='gender',
        ),
        migrations.AlterField(
            model_name='transaction',
            name='balance_after_transaction',
            field=models.DecimalField(decimal_places=2, max_digits=12, null=True),
        ),
    ]
