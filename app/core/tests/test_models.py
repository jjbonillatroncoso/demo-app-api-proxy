from django.test import TestCase
from django.contrib.auth import get_user_model
from core import models

def sample_user(email='test@apimoney.com', password='testpass'):
    """Create a sample user"""
    return get_user_model().objects.create_user(email, password)


class ModelTests(TestCase):
    def test_create_user_with_mail_successful(self):
        email = "test@bankapi.com"
        password = "test123"
        user = get_user_model().objects.create_user(email, password)
        self.assertEqual(user.email, email)
        self.assertTrue(user.check_password(password))

    def test_new_user_email_normalized(self):
        email = "test2@BANKAPI.COM"
        user = get_user_model().objects.create_user(email, "test123")
        self.assertEqual(user.email, email.lower())

    def test_new_user_invalid_email(self):
        with self.assertRaises(ValueError):
            get_user_model().objects.create_user(None, "test123")

    def test_create_new_superuser(self):
        user = get_user_model().objects.create_superuser("test@bankapi.com", "test123")

        self.assertTrue(user.is_superuser)
        self.assertTrue(user.is_staff)
    
    def test_initial_amount_0(self):
    # test if account is created with 0 balance
        acct = models.UserBankAccount.objects.create_account(user=sample_user())
        self.assertEqual(acct.balance,0)

    def test_no_funds_available(self):

        with self.assertRaises(models.InvalidTransaction):
            dummyAccount = models.UserBankAccount.objects.create_account(user=sample_user())
            models.Transaction.objects.create_transaction(dummyAccount,5,transaction_type=2)
              
    def test_invalid_account(self):
        with self.assertRaises(models.InvalidTransaction):
            models.Transaction.objects.create_transaction(None,5,transaction_type=2)

    

    # account cant make transaction as other account

    # reject withdrawals if there is no money

    # reject transaction if address is incorrect or null 

    # sum of transaction for user must be equal to account balance 

    # account cant deposit money to itself 

