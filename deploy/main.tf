terraform {
  backend "s3" {
    bucket         = "demo-app-api-devops-tfstate"
    key            = "demo-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "demo-app-api-devops-tf-state-loc"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.50.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}
data "aws_caller_identity" "current" {}