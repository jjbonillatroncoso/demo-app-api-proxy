# Django rest framework bank app

.

The following app permits to user withdraw and deposit money to their own accounts

The app has the following assumptions

 - No money transfers between accounts
 - Transactions can only be performed by the user who owns the account
 - Superuser can bypass limit using django admin

## Architecture notes

 - Code can be deployed through git lab by two stages staging and production
 - Stages don't share cloud resources 
 - local testing and deployment on Git lab is done through containers user docker compose

## Use notes
 - Get access to posstman workspace
 - Create a user
 - Get token with user credentials
 - Create accounts for user
 - Create transactions for user´s account


## Postman environment 
 - Development: {{host}} variable is assigned to 127.0.0.1:8000
 - Staging: {{host}} variable is assigned to https://api.staging.cloudtechsolution.net
 - Production: {{host}} variable is assigned to https://api.cloudtechsolution.net
 
